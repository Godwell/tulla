/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tulla.tulla.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Tr
 */
@Entity
@Table(name = "user_details")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserDetails.findAll", query = "SELECT u FROM UserDetails u"),
    @NamedQuery(name = "UserDetails.findByDetailId", query = "SELECT u FROM UserDetails u WHERE u.detailId = :detailId"),
    @NamedQuery(name = "UserDetails.findByFirstName", query = "SELECT u FROM UserDetails u WHERE u.firstName = :firstName"),
    @NamedQuery(name = "UserDetails.findByLastName", query = "SELECT u FROM UserDetails u WHERE u.lastName = :lastName"),
    @NamedQuery(name = "UserDetails.findByDateOfBirth", query = "SELECT u FROM UserDetails u WHERE u.dateOfBirth = :dateOfBirth"),
    @NamedQuery(name = "UserDetails.findByPostalAddress", query = "SELECT u FROM UserDetails u WHERE u.postalAddress = :postalAddress"),
    @NamedQuery(name = "UserDetails.findByNationalId", query = "SELECT u FROM UserDetails u WHERE u.nationalId = :nationalId"),
    @NamedQuery(name = "UserDetails.findByGender", query = "SELECT u FROM UserDetails u WHERE u.gender = :gender")})
public class UserDetails implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "DETAIL_ID")
    private Long detailId;
    @Size(max = 50)
    @Column(name = "FIRST_NAME")
    private String firstName;
    @Size(max = 50)
    @Column(name = "LAST_NAME")
    private String lastName;
    @Size(max = 50)
    @Column(name = "DATE_OF_BIRTH")
    private String dateOfBirth;
    @Size(max = 200)
    @Column(name = "POSTAL_ADDRESS")
    private String postalAddress;
    @Size(max = 20)
    @Column(name = "NATIONAL_ID")
    private String nationalId;
    @Size(max = 20)
    @Column(name = "GENDER")
    private String gender;

    public UserDetails() {
    }

    public UserDetails(Long detailId) {
        this.detailId = detailId;
    }

    public Long getDetailId() {
        return detailId;
    }

    public void setDetailId(Long detailId) {
        this.detailId = detailId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(String postalAddress) {
        this.postalAddress = postalAddress;
    }

    public String getNationalId() {
        return nationalId;
    }

    public void setNationalId(String nationalId) {
        this.nationalId = nationalId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (detailId != null ? detailId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserDetails)) {
            return false;
        }
        UserDetails other = (UserDetails) object;
        if ((this.detailId == null && other.detailId != null) || (this.detailId != null && !this.detailId.equals(other.detailId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tulla.tulla.entities.UserDetails[ detailId=" + detailId + " ]";
    }
    
}
