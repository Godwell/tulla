package com.tulla.tulla.repository;

import com.tulla.tulla.entities.UserDetails;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<UserDetails, Long> {

    //findBy<id>.

    List<UserDetails> findBynationalId(String Id);
}
