package com.tulla.tulla.resource;

import com.tulla.tulla.Service.DetailsService;
import com.tulla.tulla.Service.FIleUploadService;
import com.tulla.tulla.Service.FileStorageException;
//import com.tulla.tulla.repository.UserRepository;
import com.tulla.tulla.entities.UserDetails;
import com.tulla.tulla.repository.UserRepository;
import com.tulla.tulla.utils.ResponseWrapper;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.List;

@CrossOrigin
@RestController
public class FileUploadResource {

    @Autowired
    FIleUploadService fileService;
    //@Autowired
    //private UserRepository userRepository;
    @Autowired
    private DetailsService detailsService = null;
    @PostMapping("/upload")
    public ResponseEntity<ResponseWrapper> uploadPassport(@RequestParam("upload") MultipartFile file) {
        ResponseWrapper<String> wrapper = new ResponseWrapper<>();
        BufferedReader br = null;
        String encoding = "UTF-8";
        BufferedReader br2 = null;
        String cvsSplitBy = ",";
        String line = "";

        try {

            String location = fileService.storeFile(file);

            String ext = FilenameUtils.getExtension(location);

            if(ext.contains("csv")){
                try{
                    br2 = new BufferedReader(new InputStreamReader(new FileInputStream(location), encoding));
                    while ((line = br2.readLine()) != null) {
                        line = line.replaceAll(",,", ",NA,");
                        String[] object = line.split(cvsSplitBy);
                        String firstName = object[0];
                        String lastName = object[1];
                        String dateOfBirth = object[2];
                        String postalAddress = object[3];
                        String nationalId = object[4];
                        String gender = object[5];

                        if(firstName.equals("FirstName")){
                            //skip the first line
                        }
                        else {
                            //check if ID number exists in db

                            List<UserDetails> usr = detailsService.findBynationalId(nationalId);

                            if(usr == null || usr.isEmpty()){

                                //validation of fields
                                String message = "";
                                if(firstName == null || firstName.equals("")){
                                    wrapper.setCode(400);
                                    wrapper.setMessage("First Name is Required");
                                    return ResponseEntity.ok(wrapper);
                                }
                                if(lastName == null || lastName.equals("")){
                                    message = "Last Name is Required";
                                }
                                if(dateOfBirth == null || dateOfBirth.equals("")){
                                    message = "Date Of Birth is Required";
                                }
                                if(nationalId == null || nationalId.equals("")){
                                    message = "National Id is Required";
                                }

                                if(gender == null || gender.equals("")){
                                    message = "First Name is Required";
                                }

                                if(StringUtils.isEmpty(message)) {
                                    wrapper.setCode(400);
                                    wrapper.setMessage(message);
                                    return ResponseEntity.ok(wrapper);
                                }

                                else {
                                    UserDetails userDetails = new UserDetails();
                                    userDetails.setFirstName(firstName);
                                    userDetails.setLastName(lastName);
                                    userDetails.setDateOfBirth(dateOfBirth);
                                    userDetails.setPostalAddress(postalAddress);
                                    userDetails.setNationalId(nationalId);
                                    userDetails.setGender(gender);
                                    detailsService.save(userDetails);
                                    //return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
                                    wrapper.setCode(200);
                                    wrapper.setMessage("Success");
                                    return ResponseEntity.ok(wrapper);
                                }
                            }
                            else{
                                wrapper.setCode(400);
                                wrapper.setMessage("ID Number Exists");
                                return ResponseEntity.ok(wrapper);
                            }


                        }

                    }
                }
                catch (IOException e){
                    e.printStackTrace();
                }

            }
            else {
                wrapper.setCode(400);
                wrapper.setMessage("file format not supported");
                return ResponseEntity.ok(wrapper);
            }
//            process file here

            return ResponseEntity.ok(wrapper);
        } catch (FileStorageException ex) {
            //return "failed";
        }
        return ResponseEntity.ok(wrapper);

    }
}
