package com.tulla.tulla;

import com.tulla.tulla.Service.FileStorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableConfigurationProperties({
		FileStorageProperties.class
})
public class TullaApplication {

	public static void main(String[] args) {
		SpringApplication.run(TullaApplication.class, args);
	}

}
