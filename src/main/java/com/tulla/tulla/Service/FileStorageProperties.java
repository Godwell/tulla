/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tulla.tulla.Service;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 *
 * @author gmogoi*
 */
@ConfigurationProperties(prefix = "file")
@Component
public class FileStorageProperties {
    private String upload;

    public String getUpload() {
        return upload;
    }
    
    

    public void setUpload(String upload) {
        this.upload = upload;
    }
}

