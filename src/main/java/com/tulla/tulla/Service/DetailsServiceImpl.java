package com.tulla.tulla.Service;

import com.tulla.tulla.repository.UserRepository;
import com.tulla.tulla.entities.UserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class DetailsServiceImpl implements DetailsService {

    @Autowired
    private final UserRepository usrRepository;

    public DetailsServiceImpl(UserRepository usrRepository){
        this.usrRepository = usrRepository;
    }

    public UserDetails save(UserDetails usrDetails) {
        return usrRepository.save(usrDetails) ;
    }

    @Override
    public List<UserDetails> findBynationalId(String Id) {
        List lst = null;
        lst = usrRepository.findBynationalId(Id);
        return lst;
    }

    //@Override
//    public List<UserDetails> findBydnationalId(String Id) {
//        List lst = null;
//        lst = usrRepository.findBynationalId(Id);
//        return lst;
//    }
//    public List<UserDetails> findById(String Id) {
//
//    }

}
