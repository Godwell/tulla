package com.tulla.tulla.Service;

import com.tulla.tulla.entities.UserDetails;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface DetailsService {
    UserDetails save(UserDetails userDetails);
    //List<UserDetails> ls findById(String Id);
    List<UserDetails> findBynationalId(String Id);
}
